projects <- unique(v$project)
f <- function(project) {
	vax <- v$kappa[v$project == project]
	vmin <- min(vax)
	vax <- sort(vax - vmin)
	c(100*vax[2]/vmin,100*vax[length(vax)]/vmin)
}
sapply(projects,f)
projects <- unique(v$project)
f <- function(project) {
	vax <- v$accuracy[v$project == project]
	vmin <- min(vax)
	vax <- sort(vax - vmin)
	c(100*vax[2]/vmin,100*vax[length(vax)]/vmin)
}
sapply(projects,f)
