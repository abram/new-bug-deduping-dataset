cd and
(for file in *.txt; 
do 
	echo $file `cat $file | fgrep Correctly | tail -n1`  `cat $file | fgrep Kappa | tail -n1` 
done) | sed -e 's/_/ \& /g' | \
sed -e 's/^and/Android/' | \
sed -e 's/^ecl/Eclipse/' | \
sed -e 's/^moz/Mozilla/' | \
sed -e 's/^off/Office/' | \
sed -e 's/ press / Pressman /' | \
sed -e 's/ swe / SWEBOK /' | \
sed -e 's/ web / Web /' | \
sed -e 's/ office / Office /' | \
sed -e 's/ dev / ProjDoc /' | \
sed -e 's/ [moe]doc / ProjDoc /' | \
sed -e 's/tree.*Instances [0-9]*//g' | \
sed -e 's/\% Kappa statistic/ \& /g' | \
sed -e 's/$/ \\\\/g' | sort
