	 120	

Chapter 6	

Posting, Sharing, and Managing Files

You can name the document library just about anything you want and include spaces,
punctuation characters, and even items that are no-nos in other names, such as percentage symbols and exclamation points. The name of the document library will also appear
in the Document Library list in the Web Parts group so that you always have the option
of adding the library to another page if you like.

Adding Documents
Now to make the library functional, you need to add some files. You can do this by
uploading files one at a time, or you can upload multiple files at once. When you’re first
creating the document library, you might want to add all the files you’ve saved using the
multiple upload feature, and then as you expand the files later, add them one at a time.
This section describes both processes.

Posting Single Documents
Here are the steps for uploading a single document:
	 1.	 Begin by clicking the Add Document link beneath the document library. (See

F
­ igure 6-2.)

FIGURE 6-2  Click Add Document to begin adding documents to your library.

	 2.	 In the Team Files – Upload Document dialog box (shown in Figure 6-3), click the

Browse button.
	 3.	 Navigate to the folder containing the file you’d like to add. Click it and click Open.
	 4.	 Click OK to close the Upload Document dialog box.

The file is added to the list, and the type, name, date the file was last modified,
and name of the person who modified the file are displayed in the document
library.

		
Creating a Document Library

FIGURE 6-3  You click Browse in this dialog box to upload a single file.

Adding Multiple Documents
If you have a number of files you want to post to your document library, you’ll find it
easier to upload them all at once. Here is how to do that:
	 1.	 Begin by clicking the Add Document link.
	 2.	 In the Team Files – Upload Document dialog box, click Upload Multiple Files. The

Team Files – Upload Multiple Documents dialog box appears. (See Figure 6-4.)
	 3.	 Using the folder tree on the left, you can drag the files you want to upload to the

blue area in the top of the dialog box or click the Browse For Files Instead link to
display the Open dialog box.
	 4.	 If you choose to browse for files, navigate to the folder containing the files you

want to add and select them.
	 5.	 Click Open to add the files.

Chapter 6	

121

	 122	

Chapter 6	

Posting, Sharing, and Managing Files

FIGURE 6-4  The dialog box that appears when you click Upload Multiple Files in the Team Files –
Upload Document dialog box.

	

Tip	

By default, the Overwrite Existing Files check box is selected so that any
files you upload will overwrite files with the same name that are already
in your document library. You’ll find that it’s a good practice to keep this
check box selected so that versions of specific files are kept to a minimum
in your site and reduce the risk of someone on your team working with
an outdated version of the file. You’ll have to communicate this policy in
advance to the team, though, so that all members understand that new
versions of files will overwrite older versions with the same name.

Organizing Document Libraries
Keeping your documents named and organized effectively will go a long way toward
helping your team find what they need when they need it. You can help this process
along by choosing specific naming conventions for the various files you post. You might
ask everyone to name files with their initials and the date in the file name, or assign
s
­ pecific codes for different departments to use so that they’ll be able to recognize their
own files easily.

		
Organizing Document Libraries

In addition to ordering files by the naming conventions you choose, you can also arrange
­
files based on the information in the document library table. What’s more, you can
c
­ ustomize the columns in the document library to include information that you feel is
most relevant to your project.

Ordering Files in Your Document Library
By default, the document library shows four columns: Type, Name, Modified, and
M
­ odified By. You can sort the files by hovering the mouse cursor over the column you
want to sort by; when the down arrow appears, click the arrow to display a list of sorting
options.
For example, if you want to sort the Name column so that the files appear ­ lphabetically
a
from A to Z, hover the cursor over the left side of the Name column header. Click
A
­ scending (as shown in Figure 6-5), and the files are alphabetized in the document
library.

FIGURE 6-5  You can sort the files in the document library so that they are easy for your team members

to find.

Chapter 6	

123

	 124	

Chapter 6	

Posting, Sharing, and Managing Files

Modifying the Current View
You can easily create your own kind of document library with the columns that best fit
the information you need to know for your specific project. For example, you might want
to add a column that shows who a file is checked out to and list the departments that are
responsible for the various files.
Begin the process of modifying the current view of the document library by clicking the
top row of the document library table. The Library Tools Library tab becomes available. Click Modify View and a list of options—which include Modify View and Modify In
SharePoint Designer (Advanced)—appears. Click Modify View. (See Figure 6-6.)

FIGURE 6-6  Click the document library, and choose Modify View in the Library Tools Library tab to

modify the library.

You can change many things about the way your files appear in your document library.
Clicking Modify View displays a screen that offers you a number of settings you can
tailor to change the way the library appears. As you see in Figure 6-7, the choices on this
screen enable you to do the following:

	 Choose the columns you want to appear in the document library.

■

	 Increase or decrease the number of columns displayed.

■

	 Choose the columns by which the information will be sorted.

■

		
Organizing Document Libraries

	 Filter the items shown in the document library according to criteria you specify.

■

	 Add a button that enables team members to edit the document library.

■

	 Display check boxes next to individual items so that members can select multiple

■

files.

	 Group similar files.

■

	 Add totals for items in the document library columns.

■

	 Apply a style to the document library.

■

	 Choose whether or not files are displayed in folders.

■

	 Set a limit for the number of items that can be displayed in the document library

■

list.

	 Set the way you want the document library to appear when viewed on a mobile

■

device.

FIGURE 6-7  You can modify the current view of your document library and change the columns, style,
number of items allowed, and more.

You can change the columns in the document library by displaying some and hiding
o
­ thers and by rearranging the order of the columns as they appear in the table. First
select the check boxes in the Display column of the items you want to appear. Then, in

Chapter 6	

125

	 126	

Chapter 6	

Posting, Sharing, and Managing Files

the Position From Left column, click the arrow and choose the number that indicates the
position in the table you want the column to have.
The Sort selection is another setting you can use to ensure that the files appear in the
order that makes the most sense for your project. You can choose a primary sort and
a secondary sort—this means the files will be sorted by the primary sort first and then
by the secondary sort. For example, if you choose Checked Out To as the primary sort,
your document library will list all documents by the person they are checked out to. If
you ­ elect Title as the secondary sort, the documents will be listed alphabetically by title
s
within the Checked Out To grouping.

Filters enable you to choose which items will appear in the document library list. For
example, you might filter the files so that only files posted in the last month appear in
the list. Or you could show only files that have been posted by certain members of your
team.

Another item you might want to experiment with as you’re setting up your document
library is the Style feature. SharePoint Online starts out displaying the library in default
view, which is a simple design with no color shading at all. The other styles available in
Modify View are Basic Table, Document Details, Newsletter, Newsletter No Lines, Shaded,

		
Organizing Document Libraries

and Preview Pane. Table 6-1 tells you a little more about the various designs you’ll get
when choosing the various library styles.
TABLE 6-1  Choosing a Document Library Style

Library Style

Description

Basic Table

The same view as Default view

Document Details

Shows each file in its own box with its file name, date modified, and
modified by information

Newsletter

Lists files separated by lines, with an Edit button for each entry

Newsletter, no lines

Lists files without lines separating them, and includes an Edit button

Shaded

Displays shading in every other row of the document library

Preview Pane

Lists the files on the left, and displays the information for each file as
you point to it

After you make your modification changes in Modify View, simply click OK to return to
your document library and take a look at the changes you’ve made. If you aren’t happy
with the changes or want to try something different, simply click the top of the document library again to display the Library Tools tab and return to Modify View to make
your additional changes.

DO YOU NEED TO USE SHAREPOINT DESIGNER?
Office 365 is designed to be easy to use and customize without a lot of technical
­
know-how. You don’t have to be a programmer to get the best use of all the
features available to you. That’s a good thing.
But what if you’re comfortable designing websites and you’d like to have a little
more design control over what goes where on the page? SharePoint Online
enables you to open your page and work with the design directly in SharePoint
Designer if you choose. You’ll find this option in the Modify View list and also as
an option in the Site Actions list.
SharePoint Designer is available as a free download from the Microsoft
D
­ ownload Center at www.microsoft.com/downloads, so if you try it and decide
not to use it after all—no harm done. You might just find, however, that it’s easy
enough to use that you enjoy tweaking your team pages and having a bit more
flexibility than the web-based design tools offer you.

Chapter 6	

127

	 128	

Chapter 6	

Posting, Sharing, and Managing Files

Adding a New Column to the Document Library
Maybe the basic layout of the table is fine and you want to leave much of the
i
­nformation the way it is. But suppose that it’s missing a column that shows which
d
­ epartment created the file. You can add a new column easily by clicking the document
library to ­ isplay the Library Tools Library tab. Then click Create Column in the Manage
d
Views group.
When the Create Column dialog box appears, as shown in Figure 6-8, type a name
for the new column and choose the item that reflects the type of information you’ll
be ­ toring in that column. For a department name, the default setting—Single Line
s
Of Text—works fine. But you can also add a picture (perhaps a team member’s profile
p
­ icture) using the Hyperlink Or Picture setting, or create a menu of choices using the
Choice item.

FIGURE 6-8  Enter the name, and choose the type of information you’ll display in the new column.

In the Additional Column Settings area, you can type an additional description and
choose whether you want to require that the new column contain information. You can
also limit the number of characters entered in the column, add the column to the default
view, and enter a formula if you want the data in the column to be validated.

		
Organizing Document Libraries

Creating a New View for Your Document Library
Although you might not need to get too fancy with the ways you store your files in
SharePoint Online, the program includes a large variety of features you can use to
c
­ ustomize the information displayed in the table. You can create your own views and
c
­ reate the document library style that best fits your needs.
Note that the view you create for your document library isn’t an either/or choice,
h
­ owever—it’s both/and. You can have a regular document library view as well as a
d
­ atasheet and calendar view, all using the same data. For example, Figure 6-9 shows a
datasheet view that has been created for the document library you saw earlier in this
chapter.

FIGURE 6-9  You can create a new view to show your files in different ways.

To create a new view for your document library, follow these steps:
	 1.	 Click the top of the document library to display the Library Tools tab.
	 2.	 Click the Library tab.
	 3.	 In the Manage Views group, click Create View.
	 4.	 In the Choose A View Format area, choose the type of view you want to create

(Standard, Calendar, Access, Datasheet, Gantt, or Custom view).
	 5.	 Type a name for the new view.

Chapter 6	

129

	 130	

Chapter 6	

Posting, Sharing, and Managing Files

	 6.	 Choose whether you’ll share this view with the public or view it yourself.
	

7.	 Specify other settings related to the view type you selected, and click OK.

Switching Views
After you create new views, you need to be able to switch among their display easily.
Here again, you go to the Library Tools Library tab, in the Manage Views group. Click the
Current View arrow to display a list of available views, and click the one you want to see.
It’s that simple.

Working with Document Library Files
After you post your files in the document library, you can easily add to them, edit them,
check them in and out, download copies of them, and set alerts so that you know
w
­ henever a new version of the file is posted to the space.
The Library Tools Documents tab appears when you click one of the files in your
d
­ ocument library. The various groups—New, Open & Check Out, Manage, Share & Track,
Copies, Workflows, and Tags And Notes—give you different sets of tools to use for editing, managing, saving, updating, and working with versions of the file. (See Figure 6-10.)
Having these various tools to choose from helps you and your team members keep
your files well organized so that everyone can stay as productive as possible by always
w
­ orking with the latest version of the file.

		
Working with Document Library Files

FIGURE 6-10  The Library Tools Documents tab has what you need to add, edit, and work with

i
­ndividual files.

In addition to using the tools in the tab, you can display a list of action choices by
h
­ overing the cursor over a file and clicking the arrow that appears on the right side
of the Name column. These choices enable you to view or edit the properties of the
file, edit the content of the file, check the file out so that you can work on it, create an
alert so that you’re notified when the file is changed, move the file to another location,
m
­ anage permissions for the file, or delete the file.

Chapter 6	

131

	

132	

Chapter 6	

Posting, Sharing, and Managing Files

Adding a New Library Folder
Although you can set the number of files listed in the document library, at some point it
becomes counterproductive to scroll through a list of 50 files, trying to find just the one
you need. Similar to the way you manage files on your home or office computer, you can
use folders in SharePoint Online to group files and make it easy to find the file you need.
To add a folder to a document library, follow these steps:
	 1.	 Display the page with the document library you want to change.
	 2.	 Click the library to display the Library Tools tab.
	 3.	 Click the Documents tab.
	 4.	 Click New Folder in the New group.
	 5.	 In the New Folder dialog box, type a name for the new folder and click Save.

The new folder appears at the top of the file list with the name you specified. You can
change the settings for the folder (for example, rename it) or remove it by pointing
to the folder name and clicking the arrow that appears on the right side of the Name
c
­ olumn.
	

Tip	

You can connect the folders you create in SharePoint Online to ­ icrosoft
M
Outlook so that you can easily synchronize your files whether you’re
working in your team site or not. Click the folder you want to link, and
click the Library Tools Library tab. When you click Connect To Outlook
in the Connect & Export group, Outlook asks you to verify that you
want to connect this SharePoint Document Library to Outlook. Click
Yes, and the login window for Office 365 appears. Enter your user name
and ­ assword, and Outlook downloads the file from the linked folder. A
p
folder showing your team site name appears in the SharePoint Lists area
at the bottom of the Navigation Pane in Outlook.

Starting a New Document
You’ll learn more about working with Office 2010 Web Apps in Chapter 8, “Working with
Office 2010 Web Apps,” but you can easily begin and work with documents directly from
the SharePoint document library. This makes it super simple to check on something in

		
Working with Document Library Files

a file, talk about changes while you’re online with your team, or just dive in and begin
creating what you’re thinking about while you’re thinking about it.
To start a new document in SharePoint Online, click the document library and click New
Document in the New group on the far left side of the Library Tools Documents tab. Click
New Document, and a new blank document opens in Microsoft Word. A message box
appears reminding you that some files may be harmful and asking you to verify that you
want to download the file. Click OK and then click Enable Editing to begin working in the
new document. These protection features are part of Office 2010 and are designed to
safeguard you against potentially harmful files you might download from the Internet.
After you enable editing on the document from your team site, however, Office adds the
site to your trusted locations and you shouldn’t need to click Enable Editing again.
	

Tip	

You can also upload an existing document from your computer to the
document library by clicking Upload Document in the New group of the
Library Tools Document tab. When you click the tool, you have the choice
of uploading a single document or multiple documents, using the same
Upload Document dialog box you used earlier in this chapter.

Viewing File Properties
File properties are simple to use in SharePoint Online. You can easily view and change the
file properties—which include essentially the file name and the title—by selecting the
check box of the file on the far left side of the document library and clicking either View
Properties or Edit Properties in the Manage group on the Library Tools Documents tab.
In the Properties dialog box that appears when you click Edit Properties, you can rename
the file or type a new title. (See Figure 6-11.) Additionally you can delete the file if you’d
like by clicking Delete Item.

Chapter 6	

133

	 134	

Chapter 6	

Posting, Sharing, and Managing Files

FIGURE 6-11  You can easily view and edit file properties to rename, retitle, or delete the file.

Checking Out and Checking In Files
When you’re working as part of a team and sharing a workspace, having a mechanism
in place that ensures people are always working with the most recent versions of shared
files is very important. SharePoint Online does this by enabling you to check files out,
work on them, and then check them in so that other members of your team can continue
the work.
When you want to check a file out, follow these steps:
	 1.	 Select the check box of the file you want to check out.
	 2.	 In the Library Tools Documents tab, click Check Out in the Open & Check Out

group.
	 3.	 A popup box warns you that you’re about to check out the file. Select the Use

My Local Drafts Folder check box if you want to store the file in a folder used to
s
­ ynchronize it with your online files.
	 4.	 Click OK.

		
Working with Document Library Files

	

Note	

If you select the Use My Local Drafts Folder check box, you might
see a message telling you that this feature will not work with Internet
E
­ xplorer until you add your SharePoint Online site to your Trusted Sites
Zone in Internet Explorer. If you get this message, cancel the checkout
o
­ peration for now, make the change in Internet Explorer, and restart
Internet ­ xplorer. Then come back and check out the file as you originally
E
i
­ntended.

You can then edit and save the file normally, and when you’re ready to check it back
in to your team site, click the File tab and choose Info. At the top of the center area of
the Info tab in Backstage view, you can see that the file is still part of the online team
site. At the top of the center column, the Office application lets you know that the file is
currently checked out, and it gives you the option of either discarding the checked-out
status (which means your changes won’t be made in the file online) or clicking Check In
to return the file to the team site. (See Figure 6-12.)

FIGURE 6-12  You can check in the file directly from the Office application you’re using.

After you check in the file, other team members can see the changes you’ve made and
work on the file themselves if necessary.

Chapter 6	

135

	

136	

Chapter 6	

Posting, Sharing, and Managing Files

Setting Alerts
Another reality of working collaboratively on a team is that you must know who is uploading what document when. Especially if you’re concerned about the deadlines you’re
facing, knowing that documents are being posted on time helps keep your stress level
down and your team moving forward.
You can set alerts in the site so that you receive a message whenever specific files are
updated or when changes are made to the team site in general. To set alerts, follow these
steps:
	 1.	 Display the document library where you want to set alerts.
	 2.	 Select the check box of a file if you want to be alerted about a specific document.
	 3.	 Click Alert Me in the Share & Track group of the Library Tools Documents tab.
	 4.	 Choose Set Alert On This Document.
	 5.	 In the New Alert dialog box, enter the names of people you want to receive the

alerts, and then choose the delivery method. (You can enter your email address or
your mobile phone number if you prefer a text message.)
	 6.	 Choose the type of changes you want to be alerted about (all changes, changes

made by others, changes made on a document you created, or changes made on
a document you last modified).
	

7.	 Choose whether you want to get alerts immediately, receive a daily summary, or

get a weekly summary. You can also choose the time of day you receive the alerts.
	 8.	 Click OK to save the alert.

	

Tip	

You can view and modify alert settings you create by choosing Alert
Me in the Share & Track group of the Library Tools Documents tab and
selecting Manage My Alerts.

What’s Next
Organizing, managing, and working with your files successfully is likely to be a major part
of your work in SharePoint Online. This chapter showed you how to set up the document libraries in your site so that they include the information you want them to display,
are easy to use and manage, and give you easy access to the types of tasks you want to
a
­ ccomplish with your files. The next chapter takes you a step further in SharePoint Online
by showing you how to keep your team moving in the right direction by creating and
managing workflows.

CHAPTER 7

Adding and
Managing Workflows
IN THIS CHAPTER:

	 Introducing Office 365
w
­ orkflows

■

	 Creating a new workflow

■

	 Using a workflow for your
p
­ roject

■

	 Managing workflows

■

WHE THE R YOU K NOW IT  or not, you use workflows all the time.
They might not be written down, and others might not follow the same
process (which might or might not cause you lots of headaches), but there
is some kind of flow to the work you’re doing. Whether you’re publishing
an annual report, getting a mailing out, launching a new Web site, or
trying to support a sales force, the tasks you complete in your day fit into
a larger work process that can be mapped and organized.

Workflows in the traditional sense are logical structures that help
you know who does what when in the work process. When you’re
working with a team, workflows can help keep a project on track
and help you see easily which team member is responsible for tasks
at any given time. SharePoint Online in Office 365 makes it easy for
you to create and apply workflows to the team projects you create.
This chapter shows you how to create and use a workflow to keep
your team engaged and working smoothly together.

Introducing Office 365 Workflows
Your SharePoint Online team site offers all kind of features and
tools you can use to keep your team on track. Creating workflows
for processes you use often—especially if they involve customer
interactions or important project tasks or documents—can help
you reduce the amount of time you spend worrying about getting
things done.
137

	 138	

Chapter 7	

Adding and Managing Workflows

To add a workflow to your team site, you need to first create a library or a list that
i
­ncludes a Choices column (which enables you to set the state of each item in the
w
­ orkflow). The basic idea is captured in the following steps:
	 1.	 You create a list of the tasks you need to accomplish to reach completion on a

particular goal.
	 2.	 You create a workflow for that list that enables you to indicate when a task has

been completed and whose involvement is needed next.
	 3.	 The workflow can notify that team member automatically by email to let him or

her know it is time for his or her task in the project.
	 4.	 When all tasks are marked as finished, the workflow is complete.

	

Note	

You might want to use the Issues Tracking list template in SharePoint
Online to create a list you can use with a workflow. You can also use
the Custom List or a document library to create a customized list that
you can use as the basis for the workflow, but for the workflow to work
properly, you must include at least one Choice field that enables you to
set multiple options. The Choice field is where you set the three different
states (Active, Resolved, and Completed) the workflow uses to track the
progress of the tasks.

The workflows you create help you easily coordinate the people, tasks, and deadlines
on your different projects. By enlisting SharePoint Online to help you track the project’s
status, you can make sure things stay on schedule and that the right people are involved
at the right points.

HOW WILL YOU USE WORKFLOWS?
A workflow does require a little forethought and planning when you set it up, but it can
save you time and trouble if you have a number of people all working on different parts
of the same process. You could use workflows to

	 Follow a call for customer service from the initial contact to the resolution

■

	 Track client inquiries about a new product

■

		
Creating a New Workflow

	 Organize the process you use to create the annual report, from brainstorming

■

to finishing the product

	 Ensure that each new employee receives training from various members of your

■

team
Whether you want to track tasks or files in a library, SharePoint Online makes
creating and updating the workflow a simple matter of changing the status of
tasks as you complete them.

Creating a New Workflow
A workflow is a process you attach to a list of steps you want to accomplish or a series of
document tasks you want to complete in a certain sequence. When you want to create a
workflow, you begin by creating the list or library you’ll use as the basis for the workflow:
	 1.	 To create the list or library the workflow will use, click Site Actions, choose View All

Site Content, and click Create. Click Library or List, and choose Issue Tracking List.
Type a name for the list or library, and click Create.
	 2.	 Click on the List Tools List tab, and click the Workflow Settings tool in the Settings

group. (See Figure 7-1.)

FIGURE 7-1  After you create an Issue Tracking list, click Workflow Settings in the Settings group of
the List Tools List tab.

Chapter 7	

139

