	 282	

Chapter 13	

Integrating All Parts of Office 365

If you have already checked out the file, when you save it, Office 365 will remind you
that the file is checked out to you and ask you whether you want to cancel the check-out
status or keep the file checked out. If you want to keep others from being able to make
changes to the file while you’re working on it, choose Keep Checked Out. If you want
others to be able to edit the file, click Discard Check Out.

	

Tip	

If a dialog box appears giving you the option to Use My Local Drafts
Folder, the file is checked out and a copy is downloaded automatically
to your SharePoint Drafts folder. This is a subfolder of your Documents
library on your computer. The folder will be synched automatically with
the SharePoint server when you access your Office 365 account again.

Saving Files to Your Document Library
After you make any necessary changes to the file offline, you can easily save your
u
­ pdated version of the file back to your team’s document library. Here are the steps for
saving the file back to the site:
	 1.	 In the application you are using (Word, Excel, PowerPoint, or OneNote), with the

file open on the screen, click the File tab.
	 2.	 Click Save & Send.
	 3.	 Click Save To SharePoint.
	 4.	 Click the folder that represents your SharePoint document library. (See

Figure 13-3.)

		
Getting Productive with Office 365

FIGURE 13-3  Choose Save To SharePoint, and select your document library to save the file back to

the site.

	

Tip	

If you don’t see your document library folder in the list of available
folders, you can click Browse for a location and enter the URL of your
document library. In the Save As dialog box, select Workspaces at the top
of the folders list, and choose the folder you need from the displayed list.
Click Save to save the document.

Chapter 13	

283

	 284	

Chapter 13	

Integrating All Parts of Office 365

Getting Productive with Office 365
Of course, Office 365 is much more than just a way to manage files that you’ll work on
in a team. As you’ve seen throughout this book, you can use the various tools in
Office 365 to

	 Create a shared team site for your group to gather and share documents and tasks

■

as you collaborate on projects.

	 Provide email and calendar tools to help you communicate and schedule meetings

■

and events.

	 Access Office Web Apps so that you can work on your files from any point you

■

have web access.

	 Offer secure instant messaging and online meetings for your group.

■

	 Design and manage a public-facing website for clients and prospective customers.

■

Using these different tools together gives you a comprehensive way to manage your
team and your tasks in the cloud. The sections that follow offer several examples of the
way you can use the various elements of Office 365 to complete specific business-critical
projects.

Creating an Annual Report
An annual report is an important tool that communicates to those interested in your
business or organization how you have fared during the past year. The general idea is to
present things in a positive light, showing examples of tasks you’ve mastered, obstacles
you’ve overcome, and markets you’ve grown into.

Thinking Through Your Content
People who read your annual report might be your stockholders, investors, or donors—
often they are people who have a vested interest in what you’re doing, which means you
need to aim for quality. A good annual report might include the following information:

	 An introduction from the CEO or president of the board

■

	 An overview of the year

■

	 Specific sections about new products or services

■

	 A section about new capital improvements to your facility

■

		
Creating an Annual Report

	 Biographies or introductions for new staff members

■

	 A financial section that explains sales results and projections

■

	 A brief history of your organization

■

	 Expectations for the coming year

■

Planning for Production
After you know what types of content you want to be included in the report, think
through how you plan to deliver it. Will you produce a PDF to hand out at a company
meeting? Do you want the PDF to be available as a download from your Office 365
p
­ ublic website?
Do you want to create a PowerPoint presentation of the report—or at least relevant sales
data—to make available online or to show during the meeting?
The ways in which you distribute the information you create will largely be determined
by the tools you use to prepare it. You can use Word to create a PDF, PowerPoint to
c
­ reate a presentation, and OneNote to collect your team notes along the way.

How Office 365 Helps Facilitate the Process
The tools in Office 365 will help you in many different ways as you pull together the
v
­ arious pieces for this project. One of the first things you need to do, of course, is choose
a deadline that gives you the time you need to complete the project and leaves enough
time for you to correct anything that goes wrong before the report needs to go out.
You can also decide who you want to be on the report team and make sure they have
the necessary licenses to work with the various programs in Office 365.
After you identify your team and decide on a deadline, you’re ready to begin the actual
day-to-day work. The following list provides some ideas for ways you can use the tools in
Office 365 to complete your annual report:

	 Use OneNote to start your brainstorming  Create a OneNote notebook to store

■

team ideas as you think through things you’d like to include in the report. You can
share the notebook online through OneNote Web App and make it available in
the SharePoint document library you create. (See Figure 13-4.)

	 Create a SharePoint page for the annual report project  You can create a

■

d
­ edicated page where you can post announcements, questions, files, calendars,
and other content related to the annual report project.

Chapter 13	

285

	 286	

Chapter 13	

Integrating All Parts of Office 365

FIGURE 13-4  Create a shared OneNote notebook to gather your thoughts as your team begins to
brainstorm about the report project.

	 Create a document library for the files you’ll use in the report  This makes

■

it easy to find just what you need for this project without browsing through a
c
­ ollection of files used for other things. (See Figure 13-5.)

FIGURE 13-5  Add a new page and a document library for this specific project so that you have

everything you need on one page.

		
Tracking a Sales Promotion

	 Add a workflow to track report progress  Adding a workflow for the report

■

helps you assign specific tasks and roles for accomplishing the goal, and it helps
you to coordinate everyone’s efforts.

	 Assign tasks for different aspects of the report  By doing this, you know who

■

is doing what and you can tell where you are in the process by seeing who has
completed the tasks assigned to them.

	 Host weekly team meetings in Lync to talk about the report progress 

■

Keeping everybody moving ahead in the process—as a team—is an important
part of an overall successful effort. Weekly meetings help the whole team keep
track of how things are developing and give you a sense early on of where the
trouble spots are.

	 Begin content creation in Microsoft Word  Open your OneNote notebook, and

■

drag notes you need into your Word document draft. Share the file with others so
that all team members can add the content they are responsible for adding.

	 Create worksheet data in Excel  You can add worksheet information—­ncluding
i

■

sales figures, new hires, capital expenditures, and foundation donations—in
an Excel worksheet. You might also want to create charts that showcase data
­
and place them in the same file, perhaps on a different worksheet. You can link
­
the worksheet data to your Word draft to ensure that the most current data is
r
­ eflected in the report.

	 Spotlight sales data with PowerPoint slides  You might want to create a short

■

sales presentation in PowerPoint and include those slides as illustrations in the
­
financial pages of the ­ nnual report. You can also give the presentation in the
a
a
­ nnual meeting if you have a speaking spot on the meeting’s agenda.

Tracking a Sales Promotion
Often sales promotions are launched and tracked by the folks behind the scenes—or
the sales manager or staff—without a lot of company fanfare or communication. But
especially if you want your whole group to feel the effects of the promotion (there’s a
boost of energy, after all) and understand how increases in sales benefit the whole lot
of you, it’s a good idea to think through your promotion as a way to raise goodwill and
enthusiasm companywide.

Chapter 13	

287

	 288	

Chapter 13	

Integrating All Parts of Office 365

Thinking Through Your Approach
Your sales promotion might be a small project—just some strategy, a worksheet, and
some promotional material for your salespeople—or it might be a bigger event, with
updates on your website, blurbs for the company newsletter, and charts and progress
reports along the way.
What would you like to accomplish from your promotion? And how will you let people
know? Do you want a print component as well as an online piece? Will you be sending
out email updates? Walk through your whole strategy from start to finish, and envision
the number of people it will involve to prepare the event and pull it off.

Planning for Production
Planning for the way you want to support your sale promotion involves thinking through
the process from start to finish. For example, you’ll need to work through the following
considerations.
What will you use to let everybody know about the promotion? Think “materials” here.
For example, you might want to create an internal web page where salespeople can
go for more information. Send out a broadcast email to the team introducing the new
p
­ romotion and linking back to the internal web page.
You might also create some promotion rules that participants can download in PDF
format, create some sample sales data in Excel, whip up a few charts to show the kinds
of results you’re hoping for, and design a brochure that showcases the items salespeople
can win as a result of the promotion.
You could also create flyers to post in shared places like around the water cooler or
c
­ offeemaker, or if your office is completely virtual, you could post announcements on
your team site with links to the website for more information.

How Office 365 Can Help with Your Sales Promotion
There are as many different ways to organize a sales promotion as there are sales
p
­ romotions, but the tools in Office 365 can help you think through your approach and
put pieces in place to track sales information and share successes with your team. You
can use Word, Excel, PowerPoint, and OneNote, and create pages on your team site to
help everyone get access to the same information. Here are a few examples:

	 Draft your promotion strategy in Word  Working with your team, create and

■

share a Word document that includes your play-by-play strategy for the sales
p
­ romotion. Be sure to include the contact information for key people on the team,
as well as a timeline for the full execution of the promotion.

Tracking a Sales Promotion
		

	 Crunch the numbers in Excel  What kinds of sales results are you hoping for?

■

What are the ideal numbers you’d like to get in different regions? Do you want to
increase a specific type of sales in one area but encourage other sales in different regions? If so, think through how your promotion can help encourage the
types of growth you’d like to see in your sales staff. Put some real numbers in the
worksheet as your ideals so that you know what you’re aiming for. Set your sales
goal—even if that number is known only to your top management team—before
the promotion begins. (See Figure 13-6.)

	 Links to other sales worksheets and regions  Another consideration, if you

■

want to make sure your sales data is live and up to date, involves linking the sales
reports to the actual sales data as it comes in. You can do this by linking an Excel
worksheet to your Word document or by creating a link in your sales promotion
worksheet to other sales worksheets used for the various regions you’re tracking.

	 Include your progress in PowerPoint presentations, and make those

■

a
­ vailable on your team site  Keep people in sync with your progress by sharing
the good news as it unfolds. Create a PowerPoint presentation, and add it to your
team site. That way you can post the slides, with updated sales data, to your team
site so that everyone can see the progress that’s being made (and perhaps feel
inspired to try a little harder next week themselves).

FIGURE 13-6  Create a worksheet with sales data and projections as you prepare for your sales

promotion.

Chapter 13	

289

	 290	

Chapter 13	

Integrating All Parts of Office 365

Preparing an Online Training Module
If your team—or perhaps your entire company—works remotely in the cloud, one of
the challenges you might face is making sure everyone is trained in a similar way for
common business procedures. For example, if you’re using a specific kind of issuetracking software, how do you teach your employees to use it? Or does everyone in your
c
­ ustomer service department know how to report fulfillment problems? You can develop
online training modules to make sure that your team has access to the knowledge they
need to complete their business-critical tasks.

Thinking Through Your Training
Envisioning the type of training you need is an important part of designing a module
that hits the mark. There are a number of questions you can ask here, including the
f
­ ollowing:
■	

Who will be using the training module?

■	

Will they have access to the web?

■	

Will they be able to call in for a conference call while watching a presentation?

■	

Do you want the training to be self-directed so that participants can work through
it on their own?

■	

Do you want any kind of evaluation method at the end of the training?

■	

How will you know who has completed the training?

Planning for Production
As in the prior example, way you deliver the training will be largely determined by
the type of content you create. If you design your learning module in PowerPoint, for
example, you can add audio as a voiceover to give instructions as the participant clicks
through the slides. You can also include video to demonstrate key processes.
You cannot, however, use PowerPoint’s broadcast feature (new in PowerPoint 2010) to
broadcast a presentation complete with sound and video. For now, the broadcast feature
supports slides only.
You could create your learning module in a web page on your site and include a mix of
documents and presentations. For example, the participant could read an introduction,
open a word file to do an exercise, watch a video clip or presentation to learn more, and
log their time and email in an Excel worksheet.

		
Preparing an Online Training Module

How Office 365 Can Help with Your Online Training
One great way to approach training—especially if you think you might do a lot of it—is
to create a Training page on your team site. On this page, you could add subpages with
a focus on different training modules: Sales, Technology, New Hires, and so forth. After
you have an overall idea of your training program in mind, you can get to work using
O
­ ffice 365 to create the pieces. Here are other ideas:

	 Get your team together to brainstorm in Lync  Online training can be a fun,

■

creative project that includes the talents and interests of many people on your
team. Create an online meeting in Lync, and invite your team to a whiteboard
brainstorming session. You can all talk about what you’d like to see in the training
and identify the training goal as well as your primary objectives.

	 Outline the process in a Word document or OneNote notebook  The idea

■

here is to give yourself space to identify all the important steps as you begin to
pull together the content for the training.

	 Use Word to draft a ­ ocument of your training content  Focus first on the
d

■

outline and the content you want to convey; add illustrations as needed (or create
a separate file or folder on your SharePoint site for photos you want to use).

	 Use your OneNote notebook to collect research you will use as the basis for

■

your training  Be sure to include full resource citations and links if appropriate.

	 Build a prototype of your training in PowerPoint  As a first draft, you can in-

■

clude the content and photos (or video clips) your participants will see. (See Figure
13-7.) Share the draft with your team, and use Lync to get together and talk about
the draft.

	 Save as a broadcast, and save as video  If the training module you create is not

■

interactive but walks the participant through a process with audio and video, you
can save the presentation as a video clip and post it to your team site.

	 Use Word to create an assessment tool  This helps participants see what they

■

learned and let you know what they thought about the training module.

	 Make handouts and tip sheets available on the training page  If you do this,

■

participants can download additional information if they want to review the content that was covered.

Chapter 13	

291

	 292	

Chapter 13	

Integrating All Parts of Office 365

FIGURE 13-7  Create a prototype of your training module in PowerPoint.

Happily Ever After…in the Cloud
In this chapter—and throughout this book—you’ve been invited to think about how
O
­ ffice 365 can help you and your team reach your goals, whether you work in the
same office building or you are scattered across the globe. Yesterday’s water cooler has
b
­ ecome today’s Lync chat, and the old processes we used to follow—mailing reports,
making long-distance phone calls, flying to meetings—have been replaced (thankfully)
with lower-cost, lower-impact options.
Today, using the tools in Office 365, you can build a connected and motivated team that
has everything it needs to complete quality projects, anytime and anywhere.
Good luck, and keep finding new ways to be creative and stay in sync with Office 365!

APPENDIX A

Extras for Great
Teams
E V E RY G R O U P   is different, just like every cloud is different. So it
l
­ogically follows that every group in every cloud will be different. One
thing that all groups share, however, is that to accomplish the work they
come together to complete, they need to have specific, measurable ways
to assign tasks, track their progress, get their questions answered, and
reach their goals.

This appendix offers ideas to help you organize and manage the
overall flow of information in your site. These simple forms aren’t
meant to replace any of the electronic processes you can use in
Office 365, of course. You can use workflows to automate your
project review process, for example, or use Microsoft Lync to hold
weekly meetings where you check in on various stages of your
responsibilities.
But if you’re just starting out organizing your team, you might find
a few helpful ideas here for gathering your data and keeping it
organized and easy to find in the cloud.

293

	 294	 Appendix A	

Extras for Great Teams

Thinking Through Your Group Process
Early in this book, you read about the basic steps of a group process—forming, storming,
­
norming, performing, and adjourning. As you think through the various stages in your
project, consider the type of information you want to track:

	 What is the scope of your project?

■

	 How long will the project last?

■

	 What will the benchmarks of your project be?

■

	 Will you set internal deadlines for the project? (For example, you might set a date

■

that serves as a deadline for having 50 percent of the project completed.)

	 What are the roles your members will perform?

■

	 How will you stay in touch with each other (and with what frequency)?

■

	 Will you use workflows to track your progress?

■

	 How will you notify the team when a problem arises?

■

	 How will plan changes be communicated?

■

	 How will you assess the success of your group?

■

Team Contact List
You can reach all your team members though your contacts list in Lync and the Admin
User page in Office 365, but you might also want to create a contact list with complete
names, email addresses, site addresses, responsibilities, and more that you download and
make available to the team. (See Figure A-1.)

		
Excel Worksheet with Licenses and Permissions

FIGURE A-1  You can create a contact log file so that you can view all user accounts and permissions at a

glance.

Excel Worksheet with Licenses and Permissions
Office 365 keeps track of the licenses and permissions you assign to your team members.
Depending on the programs you use and the version of Office 365 you’ve selected, you
might have 25 licenses—or more—to assign. If you’re working with a large team and
want to make sure members have access only to the programs and services they need,
you can use an Excel worksheet to manage and track the licenses you’ve assigned.

Meeting Agenda Template
Will you host team meetings regularly using Microsoft Lync Online? Lync makes it easy
to chat and talk in real time, share programs, review progress, and more. You can help
organize your team meetings and make sure they are effective by designing and distributing a meeting template members can use to add their own agenda items. Doing this
can help you make sure everyone is on the same page. (See Figure A-2.)

Appendix A	 295

