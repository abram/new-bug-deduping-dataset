	 258	

Chapter 12	

Designing Your Public Website

Setting Up Your Site
Some of the choices you’ll make early on as you design your site will affect the entire
layout of the public pages. Click the Design tab, and click Setup in the Site group to
display the Setup dialog box. (See Figure 12-3.) Click the Page Width arrow, and choose
the width of the page you want to display; click the Page Alignment arrow, and choose
Center, Right, or Left.

FIGURE 12-3  Use the Setup options to set the page width and alignment for your entire website.

In the Display Options area, you can choose the way the background will be displayed
on the site. Additionally, you can add the Bing search box to the site and enable the
M
­ ember Login button so that those who are part of your Office 365 team can log in
from the public site.
	

Tip	

What’s the best page width? When you’re designing for the web, you
need to think about the whole range of users who will likely be browsing
to your site. Although many desktop monitors now have larger screen
resolutions (perhaps up to 1440 x 990 or so), netbooks, laptops, iPads,
and other devices have smaller screens. For best results, leave the default
setting (980 px) or choose the smaller width (780 px) to accommodate
your visitors with smaller screens.

		
Getting Started with Your Public Website

Applying a Page Background
One big change you can make to your page right away involves changing the look of the
page background. Start by clicking the Background tool in the Page group on the Design
tab. In the Page Background dialog box (shown in Figure 12-4), add a background image
by following these steps:
	 1.	 Select the Use Background Image check box.
	 2.	 Click Select, and click Browse For An Image.
	 3.	 Navigate to the folder storing the file you want to add, select it, and click Insert

Image.
	 4.	 Back in the Page Background dialog box, click the Position arrow and choose

where on the page you want the picture to appear.
	 5.	 Click the Tiling arrow, and choose whether you want the image to be tiled

(­ eaning it will be repeated as needed to fill the page) and, if so, how.
m
	 6.	 Click OK to save your choices.

	

Tip	

By default, Office 365 selects the Optimize My Image check box when
you upload a picture to your site. Optimizing an image resizes and
c
­ ompresses it so that the picture loads more quickly on your website
(which your visitors will appreciate).

FIGURE 12-4  You can add an image to the background of your page by clicking Background in the Page

group of the Design tab.

	

Note	

The background image you apply to your page will appear only in the
center text area; the navigation panel and header areas are controlled by
other formatting choices.

Chapter 12	

259

	 260	

Chapter 12	

Designing Your Public Website

Choosing a Theme and Header Style
The way you design your site—with the colors, images, and navigation style—creates a
kind of experience for your site visitors that communicates something about your organization and your services. If the colors are dark and the fonts are conservative, people
might see your organization as being very serious. If the site is colorful and the fonts are
playful, visitors might view your company as creative or whimsical.
You can choose a theme in your Office 365 site to apply a set of colors and a logo design
for the site. Office 365 includes a huge range of theme choices that are designed to
correspond with the type of business you are running. Even though there are a number
of categories, you can choose whatever appeals to you—such as a Scenic & Landscape
scene for your computer tech company—as long as you like the look and you feel it fits
the overall tone you’re trying to convey.
To choose a theme for your site, click the Design tab and click Theme in the Header
group. A huge list of choices appears, as you see in Figure 12-5. Point to the various
categories to see the galleries of styles you can apply to your site. When you find the
one you want, click it and Office 365 applies it to your site.

FIGURE 12-5  Click Theme in the Header group to change the theme of your site.

		
Choosing a Theme and Header Style

If you don’t like the effect, you can change the theme and experiment with many other
looks. Just remember to keep your visitors in mind, which means you should consider
not only your personal color preferences but also the overall site design and how well (or
not) it matches the message you want your visitors to receive.

WHAT’S IN A THEME?
Not all themes are created equal. Each theme you select in Office 365 has been
professionally designed to present a specific look and feel. All items in the theme
are coordinated so that they look good together and provide a professional,
polished look for your site.

Changing the Site Header

The site header is the area at the top of your webpages that gives a consistent look and
feel to your site. The header is repeated from page to page so that site visitors recognize
the continuity throughout your site.
To change the header, click the Design tab and click Text in the Header group or click
the header to display the Header dialog box. In the Header dialog box (shown in
F
­ igure 12-6), type the text you want to appear as the header for the site, and use the
formatting tools to change the style and font of the text.

FIGURE 12-6  Enter a heading and a description for your site in the Header dialog box.

Chapter 12	

261

	 262	

Chapter 12	

Designing Your Public Website

You can also add a site slogan, which is a description that will appear below the header,
by clicking in the Site Slogan box and typing your description. Again, format it as you’d
like, and click OK to return to the site and view your changes.

Add a Logo to Your Site
If you have a customized logo saved as a .jpg or .bmp file, you can easily add it to your
site in Office 365. The process is simple—it’s similar to adding a photo or piece of clip art
to your pages. Here are the steps:
	 1.	 Click Text in the Header group of the Design tab.
	 2.	 Click the Logo tab.
	 3.	 Click Upload Pictures if you need to add your logo image to the dialog box.
	 4.	 Click the image you want to use as the logo. (See Figure 12-7.) If you decide you

want to forgo the logo altogether, click No Logo.
	 5.	 In the Display Options area, select whether you want the logo to appear at the top

of each page of the site or beside the page title.
	 6.	 In the Size area, choose Small, Medium, or Large for the size of the logo.
	

7.	 Click OK to save your changes.

FIGURE 12-7  Add a logo using the Logo tab in the Header dialog box.

		
Choosing a Custom Color Scheme

WHAT MAKES A GOOD LOGO GOOD?
Are there some characteristics that “good” logos have that other logos don’t?
Think of the logos you’re most likely to recognize. Chances are that you see them
everywhere—Coke, IBM, Nike, Dell. What do they all have in common? Colors
we remember. A simple, strong design. Bold characters. Think about what makes
your logo memorable in the minds of your site visitors, and be sure to use your
logo consistently on all printed and web materials.

Choosing a Custom Color Scheme
Similar to the way in which your theme creates an experience for your visitors, the color
scheme you select says a lot about your business or organization. Red is a high-energy
color; blue is passive; green is relaxing; yellow is stimulating. (Be careful about how much
yellow you use because a little goes a long way and used to excess could make your
page more difficult for visitors to read.) Visit some of your favorite sites. How do they use
color? How does the design make you feel?
You can easily apply a new color scheme by following these steps:
	 1.	 Click the Design tab.
	 2.	 Click the Color tool in the Site group on the far left end of the Design tab. (See

Figure 12-8.) A list of color palettes appears.
	 3.	 Click the color palette you like. The colors are immediately applied to the theme

you selected, overriding any other colors in use.

Chapter 12	

263

	 264	

Chapter 12	

Designing Your Public Website

FIGURE 12-8  Choose a color palette, and select a coordinated look for your public website.

Adding and Formatting Text
What do you want to say on your website? Most businesses try for a mix of friendly and
factual, welcoming visitors and making sure they are able to find what they’re looking
for with the fewest number of clicks possible. For example, if you’re hoping visitors will
download a copy of your product catalog—and you think people will likely be coming
to your site to do that—feature the catalog somewhere prominently on your site (either
on the Home page or as a link that is plainly visible to visitors) so that they can find what
they need and download it quickly. This gives visitors a good feeling about your site,
which means they’re likely to come back.
There’s a delicate balance about how much text you want on your site, however. Even
though you want visitors to find what they need, you don’t want them to wear themselves out by reading too much about it. So keep your content short and sweet. If you
must go into great detail about a process or a product, create a new page the user can
visit if they decide they want to read more about it.
It’s a simple thing to add text on your Office 365 pages, but there’s no Undo tool to undo
your changes—so it’s a good idea to be certain about your edits before you make them.
As mentioned earlier in this chapter, the easiest way to add content is to click the zone
where you want to change the text, delete what’s there, and add your new text. After
you’ve added your new content, you can use the various formatting tools to give it just
the look you want.

		
Adding and Formatting Text

Formatting Your Headings
Your pages include placeholder text in each of the zones on the pages you work with.
You can choose to use the format already there or change it to fit the tone of your site.
Here’s how to change the look of the zone headings on your page:
	 1.	 Display the page you want to change.
	 2.	 Highlight the text you want to use as a heading.
	 3.	 In the Home tab, click the Font arrow and choose a font from the displayed list.
	 4.	 Click Bold, Italic, or Underline as desired.
	 5.	 Click the Font Size arrow, and choose a size from the list. (See Figure 12-9.)
	 6.	 Click the Font Color arrow, and choose a color from the palette.

FIGURE 12-9  The font sizes in the Font Size list show both the HTML sizes and the more familiar
point sizes for the text.

	

Note	

Text sizes in HTML are shown in size levels ranging from 1 to 7, with 1
being the smallest and 7 being the largest. In print documents, text size
is generally shown in points, a typographical term used to indicate the
height of a character on a printed line. Because 72 points equals one
inch, a 72-point letter will be one-inch tall on the printed page.

Chapter 12	

265

	 266	

Chapter 12	

Designing Your Public Website

ATTACHING A CSS STYLE SHEET
If you are accustomed to working with web design, you might have a cascading style
sheet (CSS) you’ve used for other web projects in your business that you’d like to apply
to your Office 365 site.
You can add the CSS code directly to your site by clicking the Design tab and clicking
Style Sheet in the Advanced group. In the Style Sheet dialog box, you can paste the CSS
code you want to include on the site. Select the Apply Custom CSS Code To My Web
Site check box, and click OK. The new code is added to your site, and you can add CSS
styles when you use the HTML module to enter code directly to the site.

Creating Lists
Chances are you know all about bulleted and numbered lists. You use the tools in your
word processor, you see them in PowerPoint—you know the drill.
Bulleted and numbered lists are nice to use on websites because they offer information
in a succinct, clear format. The reader’s eye is drawn to scan a list quickly, and that adds
to a feeling of effective communication on the site.
When you want to create a bulleted list on your Office 365 webpage, click in the zone
where you want to add the list, click the Bulleted List tool in the Paragraph group of the

		
Adding and Formatting Text

Home tab, and type your list. Each time you press Enter, the cursor moves to the next
line and a bullet is inserted. Click the tool again to turn the format off. You can also turn
existing text into a list by selecting it and then clicking the Bulleted List tool.
A numbered list works similarly. Click the tool and type your list, or select the text and
click the tool.
	

Tip	

In HTML, a bulleted list is coded as <UL>, which stands for unordered list.
The list gets its name from the fact that the items in the list can appear in
any order. In contrast, an ordered list, or <OL>, is the designation used for
a list that must appear in a particular order. In Office 365, an ordered list
is called a numbered list.

Adding Hyperlinks
Everything you’ve done so far—add and format text, choose a color scheme, apply a
theme, customize the header—is an important part of creating a good experience for
your site visitors. But without hyperlinks, the links that take people from one page to
another, nothing on the web would work. Links bring visitors to your site and help them
find what they need while they are there.
You can add hyperlinks easily to your Office 365 pages. Here’s how:
	 1.	 On the page, highlight the text to which you want to apply the link.
	 2.	 Click the Insert tab, and click Hyperlink.
	 3.	 In the Insert A Link dialog box (shown in Figure 12-10), select the type of link you

want to create:

	 Click Web Site if you want to enter the address of a site you want to link to.

■

	 Click Page On My Site if you want to link to one of the other pages in your site.

■

	 Click My Documents if you want to create a link that enables a user to open or

■

save a document.

	 Click E-mail Address if you want to create a link that opens an email window.

■

	 4.	 For example, if you click Web Site, a Link box will appear. You can click in the Link

box, and type the web address of the page, file, or document you want to use as
the target of the link.

Chapter 12	

267

	 268	

Chapter 12	

Designing Your Public Website

	

Note	

The Link option changes depending on the type of link you select. If
you choose Page On My Site, you’ll see a list of pages so that you can
select the page in your site you want to use. If you click My Documents,
files from your document library appear so that you can choose the
d
­ ocument you want to link to.

	 5.	 If you want the target of the link (another site, a page in your own site, an email

window, or a document) to open in a new window on top of your website window,
select the Open Link In A New Window check box.
	 6.	 Click OK to save your changes and create the link.

	

Tip	

You should open the target of the link in a new window if the link leads
visitors either away from your site or to a page where they are likely to
jump to another site. When you select Open Link In A New Window, your
website stays open on the user’s screen while the new window opens on
top of it. When the user is finished viewing the target of the link, your site
is still displayed.

FIGURE 12-10  To add a link to your page, select the text you want to link and click Hyperlink.

		
Inserting, Formatting, and Aligning Images

Inserting, Formatting, and Aligning Images
As the web has matured, people have come to expect more pictures on web pages. This
is part design and part function. Pictures can showcase products, illustrate services, and
introduce important personalities. Pictures also give the reader’s eye a rest and keep a
page from feeling too full of text.
What kinds of images will you show on your web pages? You might want to consider the
following:

	 Product photos

■

	 Staff photos

■

	 Service-related images

■

	 A photo of your facility

■

	 A map to your office

■

You begin the process of adding pictures by clicking Images in the Objects group on the
Insert tab. The Insert Image dialog box appears, as you see Figure 12-11. Click whether
you want to choose images from your computer or from images you’ve already uploaded
to Office 365. Then click Browse For An Image and navigate to the folder containing the
image you’d like to add. Click Insert Image, and the image is added to your page.

FIGURE 12-11  Click Browse For An Image to add a new picture to your page.

Chapter 12	

269

	

270	

Chapter 12	

Designing Your Public Website

Formatting Your Picture
After you position your picture on the page, you might find that it needs some tweaking.
Perhaps it’s too big (like the photo in Figure 12-12). Or perhaps it comes too close to the
surrounding text. You can fix those problems—and more—by clicking the Image Tools
Format tab, which is available when the picture is selected on the page.

FIGURE 12-12  Use the tools in the Image Tools Format tab to format and align your image.

The Image Tools Format tab provides you with a set of tools you can use to format the
image on your page. In addition to working with the alignment (which is discussed in the
next section), you can do any of the following:

	 Click Change Image to replace the selected picture with a new photo you select

■

from your computer or from your uploaded images.

	 Choose Reset Image to remove all formatting changes.

■

		
Adding and Organizing Pages

	 Click Border to choose the color and thickness for a border you apply to the

■

p
­ icture.

	 Use Padding to set a margin of space around the selected image.

■

	 Click in the Height and Width boxes, and type new values to resize the photo on

■

the page.

	 Click Alt Text, and enter a description of the photo for visitors who can’t view the

■

image.
For example, the picture in Figure 12-12 clearly needs to be resized. You can do this one
of two ways: You can click one of the handles on the image and drag it in the direction
you want to resize the image. Or you can click in the Height and Width boxes in the
I
­mage Tools Format tab and type new values for the settings to resize the picture.

Setting the Alignment for Images
Next you need to think about how you want the image to align on the page. The
a
­ lignment of the image controls how—or whether—text wraps around the picture. To
change the way your picture aligns, follow these steps:
	 1.	 Click the image on the web page.
	 2.	 Click Align in the Image Tools Format tab.
	 3.	 Click No Text Wrapping, Float Right, or Float Left.

No Text Wrapping leaves the picture as is on the page and doesn’t cause the text to wrap
around the picture. If you choose Float Right, text flows around the image on the left.
Similarly, if you click Float Left, the text wraps around the image on the right.

Adding and Organizing Pages
When you’re first creating your site, four pages might seem like a lot of space to fill. As
you begin adding content and photos, you might discover that those pages fill up quickly
and you need to add more. You add pages by using the New Page tool on the Home tab.
Here’s how to do it:
	 1.	 Click the Home tab.
	 2.	 Click New Page. The Create Web Page dialog box appears. (See Figure 12-13.)

Chapter 12	

271

	

272	

Chapter 12	

Designing Your Public Website

FIGURE 12-13  Choose a template for the new web page you want to create.

	 3.	 Click a template for the type of page you want to create, and click Next.

	

Tip	

The various page templates available in the Create Web Page dialog box
each creates a specific type of page for a particular kind of information.
If you are unsure about which template you need—or don’t yet know
what you want to put on the site—click General. This template creates
a ­ eneric page you can customize later when you know what type of
g
content and layout you want to add.

	 4.	 On the Choose Page Properties page, type a title for the new page.
	 5.	 Click in the Web Address field, and modify the suggested page name if you like.

	

Tip	

If you’re creating a new page that will replace an existing one, you can
type an existing page name and select the Overwrite Existing Page check
box to replace the old page with the new one.

	 6.	 In the Navigation area, leave the check box selected if you want the page to ap-

pear in the navigation bar.

		
Selecting a Navigation Layout

	

7.	 Type the title you want to appear in the navigation bar and, if the page will be a

subpage of an existing page, click the Select Parent arrow and choose the page
from the displayed list. (See Figure 12-14.)
	 8.	 Click Finish to add the page to your site.

FIGURE 12-14  Enter page properties to assign a title, address, and navigation title for the page.

Selecting a Navigation Layout
By default, Office 365 displays the navigation panel for your site along the left side of
the page. But there are a total of three navigation layouts you can use for your site. In
addition to the Left navigation which is chosen by default, you can use the Top & Left
navigation layout or the Top navigation layout.
To choose a navigation style for your site, follow these steps:
	 1.	 Click the Design tab.
	 2.	 In the Navigation group, click the Location tool.
	 3.	 Click the Navigation style you like: Left, Top & Left, or Top. (See Figure 12-15.)

Chapter 12	

273

	

274	

Chapter 12	

Designing Your Public Website

FIGURE 12-15  The navigation style you choose for your site has a big impact on how easily visitors

can find what they need on your site.

The navigation changes are applied instantly. (See Figure 12-16.) If you want to change
the navigation style, simply repeat the steps and choose a different style.

FIGURE 12-16  The Top navigation style positions page links between the page header and the

c
­ ontent area.

		
Adding Gadgets to Your Site

Adding Gadgets to Your Site
One of the great perks of creating your website in Office 365 is that you can add a
n
­ umber of ready-made gadgets to your website. This means you can add all sorts of
interesting extras that visitors to your site will enjoy using.
The gadgets already built into your Office 365 site are available on the Insert tab, as
F
­ igure 12-17 shows. Additional gadgets are available in the More Gadgets section. To
add a gadget to your page, simply click in the zone where you want to add the gadget
and then click the gadget you want to add.

FIGURE 12-17  Insert a gadget by choosing the one you want on the Insert tab.

Figure 12-18 shows the Slide Show dialog box, which appears after you select the Slide
Show gadget.

FIGURE 12-18  Adding the Slide Show gadget.

Chapter 12	

275

	

276	

Chapter 12	

Designing Your Public Website

Enter a name for the album and then drag the photos you want to use to the panel on
the right side of the window. You can click an Add Caption prompt to add captions for
the figures you use. Click the Album Layout Style arrow, and choose whether you want to
display a basic slide show or a revolving carousel. Click OK to add the gadget to your site.
Figure 12-19 shows the Slide Show gadget on the webpage.

FIGURE 12-19  The Slide Show gadget has been added to the site.

Optimizing Your Site for Web Search Results
One major consideration as you create your site is a single question: How will people
find you?
With so many businesses and so many websites online, how will you stand out from your
competition? What will bring potential customers to your site?

		
Previewing and Publishing Your Site

You can enter keywords and your site description to help your site get noticed by search
engines such as Google, Bing, or other major search utilities. When someone enters a
word or phrase that describes what they’re searching for, if the word or phrase they enter
matches the keywords or description you’ve entered for your site, your site might appear
in that person’s search results. So the best idea is to choose keywords that reflect what
people are searching for. This helps new clients find you online.
You enter keywords and your site description by clicking the Design tab and clicking Properties. In the Choose Page Properties dialog box, click on the Search Engine
O
­ ptimization tab, as shown in Figure 12-20. Click OK to save your changes.

FIGURE 12-20  Enter your keywords and a site description in the Choose Page Properties dialog box.

Previewing and Publishing Your Site
When you’ve selected your theme and colors, added text and images, and entered
k
­ eywords, gadgets, and a site description, you might be ready to preview and publish
your site. You can take a look at the way the site will look to the public by clicking View in
the Site group of the Home tab.
If you’re happy with the way things look, you can publish your site by clicking the File tab
and clicking Save & Publish. (See Figure 12-21.)

Chapter 12	

277

	

278	

Chapter 12	

Designing Your Public Website

FIGURE 12-21  Click the File tab, and click Save & Publish to publish your site.

When you’re ready to close the website and return to your Office 365 site, click the File
tab again and choose Close The Web Page. Back in the webpage listing, you can click
Home to return to your Office 365 Home page.

What’s Next
In this chapter, you took a whirlwind tour through all the web features available in Office
365. As you can see, you have many tools for designing your site, adding content, formatting text and pictures, adding gadgets, and optimizing your site for better placement
in search engines. The next chapter closes out the book by offering examples of the ways
you can use various tools together to manage collaborative projects and reach your team
goals with Office 365.

CHAPTER 13

Integrating All Parts
of Office 365
IN THIS CHAPTER:

THROUGHOUT THIS BOOK ,  you’ve looked closely at the various

	 Using it all together—online
and off

elements of Office 365. After exploring the online interface, you followed

	 Getting Productive with
Office 365

working with document libraries and workflows, using Office Web Apps,

■

■

specific paths through administering your account, creating a team site,

	 Creating an annual report

and using Microsoft Outlook and Lync to keep your team on the same

	 Tracking a sales promotion

page and communicating well.

	 Preparing an online training
module

This chapter shines a light on the big, overarching benefit of
O
­ ffice 365—how beautifully it all works together. Here you’ll see
some examples of how using the various elements of Office 365
together can help you work effectively with your teammates to
a
­ ccomplish tasks that are important for your team.

■
■
■

Using It All Together—Online
and Off
One of the great benefits of cloud technology in all its forms is how
easy it is to work on your files and stay in touch no matter where
you might be working or how you’re accessing the web. And even
though the idea is to keep the files in the cloud so that you and
your teammates can get to them easily, you can also download and
work with versions of your projects while you’re offline and then
sync the files with the site when you log on next time.

279

	 280	

Chapter 13	

Integrating All Parts of Office 365

Checking a File Out of Your Document Library
When you want to download a file and work on it on your computer—or phone—follow
these steps:
	 1.	 Log in to Office 365.
	 2.	 Display your SharePoint team site.
	 3.	 Go to your document library.
	 4.	 Click the arrow of the file you want to work on, and click Check Out. (See

F
­ igure 13-1.)

FIGURE 13-1  You can check out a file to open a private copy and keep the file from being edited
while you’re working on it.

The file is now checked out as a private file for you to edit. Others won’t be able to make
changes to the file while it is checked out. When you’re ready to return the file to the
site—and make it available so that others can work on it—you can check it back in.

Checking a File in After You’re Done Working on It
You can also check in a file after you’ve been working on it in the application. When you
check in the file, it lets the server know that your changes have been saved and the file is
ready to be edited by others. Here’s how to check a file in:
	 1.	 Click the file you want to work with, and open it in the application used to

c
­ reate it.

		
Using It All Together—Online and Off

	 2.	 Click the File tab.
	 3.	 In the Info tab, click Check In. (See Figure 13-2.)

FIGURE 13-2  After working on a file you’ve checked out, you can return it to free use in the

d
­ ocument library by clicking Check In.

After you check in the file, it becomes available in your SharePoint document library
again so that others can open and edit the file as normal.

Saving Files to Your Computer
When you want to take a file offline so that you can work on it on your computer or your
mobile device, you can simply open the file and then use Save As to save it to your computer or device. Choose a folder where you’ll be able to find the file easily when you’re
ready to add it back to the site—for example, you could store it in your Downloads
folder, or in a folder named for the shared project you’re working on.

Chapter 13	

281

