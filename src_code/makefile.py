# Hi! I make Makefiles!
# Why? because you can run experiments in parallel!
# make -j8 ALL # will run all of the experiments with 8 threads!

CLASSPATH=".:/usr/share/java:/usr/share/java/opencsv.jar:weka.jar"
SEED = 1
mynone = "-"

import argparse

def parse_args(add_args=None):
    parser = argparse.ArgumentParser(description='Generate Makefile for the experiment')
    parser.add_argument('--SEED', default=SEED, help='Integer SEED value')
    if (add_args!=None):
        add_args(parser)
    args = parser.parse_args()
    return args

args = parse_args()
SEED = int(args.SEED)


projects = {"android":"and","moz":"moz","eclipse":"ecl","office":"off"}
top_level = ["press","swe",mynone]
mid_level = {
    "and":"OS",
    "ecl":"IDE",
    "moz":"web",
    "off":"office"
}

def mid_levels(project):
    return [mid_level[project],mynone]

low_level = {
    "and":"dev",
    "ecl":"edoc",
    "off":"odoc",
    "moz":"mdoc"
}

def low_levels(project):
    return [low_level[project],mynone]

# 1,Android,Android,Operating System,-             # w/o general context
# 2,Android,Android, -, Pressman                        # w/o domain context
# 2,Android,Android,-,SWEBOK                            # w/o domain context
# 3,Android,-, -, Pressman                                        # w/ only general context
# 3,Android, -,-,SWEBOK                                        # w/ only general context
# 4,Android,-,Operating System,-                            # w/ only domain context
# 4,Android,-,-,-                                                            # w/o any context
# 5,Android,-,Operating System,Pressman            # w/o project context
# 5,Android,-,Operating System,SWEBOK            # w/o project context

def gen_code(long_project,project,top_level,mid_level,low_level):
    """Something like
    java weka.core.converters.CSVLoader data/moz_press_IDE_mdoc.csv > data/moz_press_IDE_mdoc.arff
    java weka.filters.unsupervised.attribute.Remove -R 1,2 -i data/moz_press_IDE_mdoc.arff > data/moz_press_IDE_mdoc_red.arff
    java weka.classifiers.trees.J48 -x 10 -t data/moz_press_IDE_mdoc_red.arff>and/moz_press_IDE_mdoc_tree.txt"""
    name       = "%s_%s_%s_%s" % (project, top_level, mid_level, low_level)
    csvfile    = "data/%s_dataset/%s.csv" % (long_project, name)
    arffile    = "data/%s_dataset/%s.arff"% (long_project, name)
    redarffile = "data/%s_dataset/%s_red.arff"% (long_project, name)
    outputfile = "and/%s_tree.txt" % (name)
    # add seeds switch
    seeds = ""
    if SEED!=1:
	seeds = "-s %s" % SEED
    return {
        "out":outputfile,
        "in":[csvfile],
        "command":"\n".join(
            [        
                "\tjava -cp %s weka.core.converters.CSVLoader %s  > %s && \\" % (CLASSPATH, csvfile,arffile),
                "\tjava -cp %s weka.filters.unsupervised.attribute.Remove -R 1,2 -i %s > %s && \\" % (CLASSPATH, arffile, redarffile),
                "\tjava -cp %s weka.classifiers.trees.J48 -x 10 %s -t %s > %s" % (CLASSPATH, seeds, redarffile,outputfile)
            ])
    }


print "\nand:\n\tmkdir and||echo OK\n"
outputs = list()
for long_project in projects:
    project = projects[long_project]
    for tlevel in top_level:
        for mlevel in mid_levels(project):
            for llevel in low_levels(project):
                res = gen_code(long_project, project, tlevel, mlevel, llevel)
                outputs.append(res["out"])
                print "%s:  and %s\n%s\n" % (res["out"], " ".join(res["in"]), res["command"])


print "\nALL: %s\n" % (" ".join(outputs))
